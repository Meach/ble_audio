#ifndef USB_CDC_H
#define USB_CDC_H

#include <stdint.h>
#include "app_usbd.h"
#include "app_usbd_cdc_acm.h"
#include "nrf_drv_usbd.h"
#include "boards.h"

#define READ_SIZE 1
#define APP_USBD_CDC_ACM_ENABLED 1
#define LED_CDC_ACM_TX (BSP_BOARD_LED_2)

#define BTN_CDC_DATA_SEND 0
#define BTN_CDC_NOTIFY_SEND 1

#define BTN_CDC_DATA_KEY_RELEASE (bsp_event_t)(BSP_EVENT_KEY_LAST + 1)

#ifndef USBD_POWER_DETECTION
#define USBD_POWER_DETECTION true
#endif

#define CDC_ACM_COMM_INTERFACE 0
#define CDC_ACM_COMM_EPIN NRF_DRV_USBD_EPIN2

#define CDC_ACM_DATA_INTERFACE 1
#define CDC_ACM_DATA_EPIN NRF_DRV_USBD_EPIN1
#define CDC_ACM_DATA_EPOUT NRF_DRV_USBD_EPOUT1

static char m_rx_buffer[READ_SIZE];
char m_tx_buffer[NRF_DRV_USBD_EPSIZE];

static void cdc_acm_user_ev_handler(app_usbd_class_inst_t const *p_inst,
                             app_usbd_cdc_acm_user_event_t event);

void usbd_init();
void usbd_cdc_send(long size);
void usbd_cdc_process();

#define USBD_CDC_trm(format, args...) usbd_cdc_send(sprintf(m_tx_buffer, "[CDC] "format, ##args));





#endif